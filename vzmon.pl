#!/usr/bin/env perl

use warnings;
use strict;
use Getopt::Std;
use Data::Dumper qw(Dumper);
use Storable;

my $USER_BC  = '/proc/user_beancounters';              # parse this file TODO: fix file path 
use constant DATAFILE => '/dev/shm/zabb_vzmon_data.storable';    # and save data to this <---
use constant TIMEOUT  => 60; # timeout, where DATAFILE ist old

my %options;
my $hash;
getopts( "hdju:r:p:", \%options );
my ( $debug, $json ) = ( 0, 0 );
$debug = 1 if $options{d};
print "DEBUG: MODE ON\n" if $debug;

$json = 1 if $options{j};
print "DEBUG: -j options enable\n" if $debug and $json;
if ($json) {
    print "DEBUG: $json = 1\n" if $debug;
    &PRINT_JSON;
}

my ( $uid, $resource, $param ) = ( $options{u}, $options{r}, $options{p} ) if ( defined( $options{u} ) and defined( $options{r} ) and defined( $options{p} ) ) || &USAGE;
unless (&CHECK_CACHE) {
    $hash = retrieve(DATAFILE);
}
else {
    $hash = &PARSE_UBEANC();
}
print &GET_ITEM( $uid, $resource, $param, $hash );

exit 0;

####################################################################################################
sub GET_ITEM() {
    print "DEBUG: start fumc GET_ITEM\n" if $debug;
    my $uid      = shift;
    my $resource = shift;
    my $param    = shift;
    my $hash     = shift;
    print "DEBUG: return ${$hash}{$uid}{$resource}{$param}" if $debug;
    return ${$hash}{$uid}{$resource}{$param};
}

sub CHECK_CACHE() {
    print "DEBUG: start fumc CHECK_CACHE\n" if $debug;
    if ( !-e DATAFILE ) {
        print( "DEBUG: File", DATAFILE, "not found\n" ) if $debug;
        return 1;
    }
    my ($mtime) = ( stat(DATAFILE) )[9];    # 9 mtime last modify time in seconds since the epoch http://perldoc.perl.org/functions/stat.html
    if ( time - $mtime > TIMEOUT ) {
        print "DEBUG: check_cache return 1\n" if $debug;
        return 1;
    }
    else {
        print "DEBUG: check_cache return 0\n" if $debug;
        return 0;
    }
}

sub PARSE_UBEANC() {
    print "DEBUG: START FUNC PARSE_UBEANC\n" if $debug;
    my $CTID = {};
    my $CTID_curr;

    open( my $fh, "sudo cat $USER_BC | " ) or die "Could not open file ", $USER_BC, " $!";

    while (<$fh>) {
        print "DEBUG: input line: $_" if $debug;
        if ( $_ =~ '\s+(\d+):.*' ) {
            print "$1 add to hash CTID\n" if $debug;
            $CTID_curr = $1;
        }
        if ( $_ =~ '\s+(\w+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s*$' ) {
            print "resource=$1\theld=$2\tmaxheld=$3\tbarrier=$4\tlimit=$5\tfailcnt=$6\n" if $debug;
            ${$CTID}{$CTID_curr}{$1}{'held'}    = $2;
            ${$CTID}{$CTID_curr}{$1}{'maxheld'} = $3;
            ${$CTID}{$CTID_curr}{$1}{'barrier'} = $4;
            ${$CTID}{$CTID_curr}{$1}{'limit'}   = $5;
            ${$CTID}{$CTID_curr}{$1}{'failcnt'} = $6;
        }
    }
    print "DEBUG: return ${CTID}\n" if $debug;
    store $CTID, DATAFILE;
    return $CTID;
}

sub PRINT_JSON() {
    print "DEBUG: start fumc PRINT_JSON\n" if $debug;
    use JSON;
    my @data;
    my $hash;
    my %hash2;
    my $i = 0;
    $hash = &PARSE_UBEANC();
    for ( keys %{$hash} ) {
            unless ($_ == 0){
            my $ID = $_;
            $data[$i]{'{#CTID}'} = $ID;
            chomp( $data[$i]{'{#CTNAME}'} = `sudo vzlist -Ho hostname $ID` ) or die "Could not run $!";
            chomp( $data[$i]{'{#CTIP}'} = `sudo vzlist -Ho ip $ID | awk '{print \$1}'` ) or die "Could not run $!";
            $i++;
        }
    }
    $hash2{'data'} = \@data;
    print Dumper \%hash2 if $debug;
    print my $json_str = encode_json( \%hash2 ) . "\n";
    exit 0;
}

sub USAGE() {
    print "DEBUG: start fumc USAGE\n" if $debug;
    print "run: $0 [-d] [-j] [-h] 
    -d Enable debug
    -j Print json for zabbix lld
    -h Print this message
    -u CTID
    -r resource (kmemsize, lockedpages, privvmpages, etc) 
    -p param(held/maxheld/barrier/limit/failcnt)\n";
    exit 1;
}
